Jodel Coding Challenge
=============================

# App Design and language details:
* Language: Kotlin
* Dependency Injection: Koin
* Async Handling: Coroutine
* Navigation: Android Navigation Architecture
* Design Pattern: MVVM
* Reactive UI: LiveData, observables
* Network: Retrofit, GSON
* LocalDB: Realm DB

# UI

* ConstraintLayout
* RecyclerView
* CardView

# libraries:

* Coroutine - Aysnc Handling 
* Koin - DI
* Retrofit- Network Operations
* Gson - POJO Operation
* Glide - Image Loading
* Realm - Local DB

# APK
* https://drive.google.com/file/d/1lCe96XJfW7cTJIcix8PGPKvhqBZBXuz1/view?usp=sharing


